#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "objet.h"


OBJET creer_objet(char* nom, int qualite[],int c){
	OBJET O;
	O = malloc(sizeof(OBJET));
	if (O==NULL) exit(3);
	O->nom = nom;
	O->qualite = qualite;
	O->c = c;
	return O;
}

/*OBJET objet_ajouter_fin_liste (OBJET O[], int nbligne, int i)
{
	// Cas de la liste vide
	if (O[i] == NULL) {
		return O[i+1];
	}
	// Cas général
	OBJET Odebut = O[i];
	// Déplacer t1 pour qu'il pointe
	// sur le dernier de la liste
	while(i <= nbligne)
	{
		O[i] = O[i+1];
	}
	O[i] = O[i+1]; //on ajoute t2 à la fin de la liste

	return Odebut; //on retourne la liste, donc le premier token de la liste
}*/

void afficher_objet (OBJET O)
{
  char* nom = O->nom;
  printf(" Nom : %s \n", nom);
}

