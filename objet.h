#ifndef _OBJET_H
#define _OBJET_H


struct Objet
{
	char* nom;     // nom de l'objet
	int* qualite; // tableau des valeurs des qualités
	int c;         // cluster auquel il est affecté
};
typedef struct Objet * OBJET;

OBJET creer_objet(char* nom, int qualite[], int c);

//OBJET objet_ajouter_fin_liste (OBJET O[], int nbligne, int i);

void afficher_objet (OBJET O);


#endif
