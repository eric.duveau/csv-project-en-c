#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "objet.h"

#define MAX_LENGTH 40


int main(int argc, char* argv[]){

/*OUVERTURE DU FICHIER*/
FILE* F;

/*PARAMÈTRES ENTRÉES PAR L'UTILISATEUR EN LIGNE DE COMMANDE*/
char* fichier = "choixpeauMagique.csv"; // nom du fichier
char* nbcolonne = "6"; // nombre de colonnes (fichier)
char* nbligne = "51"; // nombre de lignes (fichier)

int col = atoi(nbcolonne); //convertie le nb de colonne en entier
int l = atoi(nbligne); //convertie le nb de ligne en entier


/*LECTURE FICHIER*/
F = fopen(fichier, "r");
if(F == NULL){
fprintf(stderr, "Erreur d'ouverture fichier %s\n", fichier);
exit(3);
}

int i = 0;
char c;
char ligne[80]; //je réserve 80 caractères pour une ligne
OBJET O[l-1];

//OBJET O[ligne-1]; // tableau d'objets
char* mot; // chaîne de caractères d'une colonne
char* tab[col]; // tableau de pointeurs
char* prenom; // prenom de l'élève
int qualite[col-2]; // tableau des scores de qualite de l'élève

/*LECTURE DE LA 1ÈRE LIGNE*/
while (c != '\n')
{
	c = fgetc(F);
}
//à ce stade on est au début de la ligne 2


int lue = 0;

while(lue != l-1){ //tant que l'on n'a pas lu toutes les lignes

	i = 0;

	fgets(ligne,80,F); //stocke la ligne dans ligne
	mot = strtok(ligne,";,'\n'"); //nom stocke la 1ère colonne de la ligne

	for (int j=0; j<col; j++) // parcours toutes les colonnes
	{
		tab[j] = malloc(strlen(mot)+1*sizeof(char)); //memoire pr une col
		sscanf(mot,"%s\n",tab[j]); //on stocke les valeurs

		printf("%s\n",tab[j]);
		mot = strtok(NULL,";,'\n'");

		if(j==0)
		{
			prenom = tab[j];
			printf("prenom : %s \n",prenom);
		}

		else if(j>0 && i<col-2)
		{
			qualite[i] = atoi(tab[j]); //on convertie la val de qualité en int
			printf("qualite[%d] = %d\n",i,qualite[i]);
			i++;
		}

	}


	for(int i = 0; i<col; i++)
	{
		printf("tab[%d]=%s \n",i,tab[i]);
	}


	O[lue] = creer_objet(prenom,qualite,0);

	printf("O[%d]->%s \n",lue,O[lue]->nom);
	printf("O[%d]->%d \n",lue,O[lue]->qualite[0]);
	printf("O[%d]->%d \n",lue,O[lue]->qualite[1]);
	printf("O[%d]->%d \n",lue,O[lue]->qualite[2]);
	printf("O[%d]->%d \n\n",lue,O[lue]->qualite[3]);


	lue++;

}

	for (int i = 0; i<l-1; i++)
	{
		printf("O[%d]->%s \n",i,O[i]->nom);
		printf("O[%d]->%d \n",i,O[i]->qualite[0]);
		printf("O[%d]->%d \n",i,O[i]->qualite[1]);
		printf("O[%d]->%d \n",i,O[i]->qualite[2]);
		printf("O[%d]->%d \n\n",i,O[i]->qualite[3]);

	}

fclose(F);
return 0;

}

